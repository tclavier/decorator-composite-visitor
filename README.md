# Décorateur

```mermaid
classDiagram
    Component <|-- ComponentImpl
    Component <|-- Decorator
    Decorator <|-- DecoratorImpl
    Decorator o-- Component
```

```mermaid
classDiagram
    Cake <|-- CupCake
    Cake <|-- Muffin
    Cake <|-- Frosting
    Frosting <|-- Chocolat
    Frosting <|-- Nuts
    Frosting o-- Cake
```
# Composite

```mermaid
classDiagram
    Component <|-- Leaf
    Component <|-- Composite
    Composite o-- Component
```

```mermaid
classDiagram
    Bundle <|-- Cake
    Bundle <|-- Assortment
    Assortment o-- Bundle
```
