package com.gitlab.tclavier.cupcake.shop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Assortment implements Bundle {
    private List<Cake> cakes;
    private Bundle[] childes;

    public Assortment(Bundle... bundles) {
        this.cakes = Arrays.stream(bundles)
                .map(Bundle::getCakes)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        this.childes = bundles;
    }

    public double price() {
        return subCakes().stream()
                .mapToDouble(Bundle::price)
                .sum() * 0.9;
    }

    private List<Cake> subCakes() {
        List<Cake> allCakes = new ArrayList<>();
        for (Bundle bundle: childes) {
            if (bundle.isAssortment()) {
                allCakes.addAll(((Assortment)bundle).subCakes());
            } else {
                allCakes.add((Cake) bundle);
            }
        }
        return allCakes;
    }

    @Override
    public List<Cake> getCakes() {
        return cakes;
    }

    @Override
    public boolean isAssortment() {
        return true;
    }
}
