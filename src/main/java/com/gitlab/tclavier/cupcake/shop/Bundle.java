package com.gitlab.tclavier.cupcake.shop;

import java.util.List;
import java.util.stream.Stream;

public interface Bundle {
    double price();

    List<Cake> getCakes();

    boolean isAssortment();
}
