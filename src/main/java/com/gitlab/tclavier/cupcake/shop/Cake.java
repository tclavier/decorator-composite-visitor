package com.gitlab.tclavier.cupcake.shop;

import java.util.Collections;
import java.util.List;

public abstract class Cake implements Bundle {

    private int stock;

    abstract boolean isDecorated();

    abstract String description();

    @Override
    public List<Cake> getCakes() {
        return Collections.singletonList(this);
    }

    @Override
    public boolean isAssortment() {
        return false;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void accept(ShopVisitor visitor) {
        visitor.collect(this);
    }

    public Integer getStock() {
        return stock;
    }
}
