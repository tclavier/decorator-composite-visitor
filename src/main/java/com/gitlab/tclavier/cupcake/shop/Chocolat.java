package com.gitlab.tclavier.cupcake.shop;

public class Chocolat extends Frosting  {

    public Chocolat(Cake cake) {
        super(cake);
    }

    @Override
    public String description() {
        return super.description() + " Chocolat";
    }
    @Override
    public double price() {
        return super.price() + 1;
    }
}
