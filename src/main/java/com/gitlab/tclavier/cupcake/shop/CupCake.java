package com.gitlab.tclavier.cupcake.shop;

public class CupCake extends Cake {

    @Override
    public String description() {
        return "CupCake";
    }


    @Override
    public double price() {
        return 2.5;
    }

    @Override
    public boolean isDecorated() {
        return false;
    }
}
