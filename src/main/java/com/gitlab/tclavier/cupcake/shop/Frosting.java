package com.gitlab.tclavier.cupcake.shop;

public class Frosting extends Cake {
    protected Cake cake;

    public Frosting(Cake cake) {
        this.cake = cake;
    }

    public boolean isDecorated() {
        return true;
    }

    public String description() {
        if (cake.isDecorated()) {
            return cake.description() + " and";
        }
        return cake.description() + " with";
    }

    @Override
    public double price() {
        return cake.price();
    }
}
