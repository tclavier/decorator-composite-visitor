package com.gitlab.tclavier.cupcake.shop;

public class Muffin extends Cake {

    @Override
    public boolean isDecorated() {
        return false;
    }

    @Override
    public String description() {
        return "Mufin";
    }

    @Override
    public double price() {
        return 4.5;
    }

    @Override
    public void accept(ShopVisitor visitor) {

    }

}
