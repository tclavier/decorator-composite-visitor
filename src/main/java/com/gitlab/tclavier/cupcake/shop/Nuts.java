package com.gitlab.tclavier.cupcake.shop;

public class Nuts extends Frosting {

    public Nuts(Cake cake) {
        super(cake);
    }

    @Override
    public String description() {
        return super.description() + " Nuts";
    }

    @Override
    public double price() {
        return super.price() + 0.9;
    }

}
