package com.gitlab.tclavier.cupcake.shop;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class PriceVisitor implements ShopVisitor {
    private Map<String, Double> report = new HashMap<>();

    @Override
    public String report() {
        String cakes = report.keySet().stream()
                .map(this::buildLineMenu)
                .collect(Collectors.joining("\n"));
        return "# Menu\n" + cakes;
    }

    private String buildLineMenu(String cakeName) {
        return "* " + cakeName + " : " + report.get(cakeName) + " €";
    }

    @Override
    public void collect(Cake cake) {
        report.put(cake.description(), cake.price());
    }
}
