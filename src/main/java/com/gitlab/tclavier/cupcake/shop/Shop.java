package com.gitlab.tclavier.cupcake.shop;


public class Shop {
    private static Shop instance;

    public static Shop getInstance() {
        if (instance == null) {
            instance = new Shop();
        }
        return instance;
    }

    public Cake speciality() {
        return new Chocolat(new Nuts(new Chocolat(new CupCake())));
    }

    private Shop() {
    }

    public Cake doubleChocolat() {
        return new Chocolat(new Chocolat(new CupCake()));    }
}
