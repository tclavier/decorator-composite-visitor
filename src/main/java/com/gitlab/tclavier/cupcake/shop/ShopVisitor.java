package com.gitlab.tclavier.cupcake.shop;

public interface ShopVisitor {
    String report();

    void collect(Cake cake);
}
