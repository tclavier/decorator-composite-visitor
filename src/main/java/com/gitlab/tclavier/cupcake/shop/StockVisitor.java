package com.gitlab.tclavier.cupcake.shop;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class StockVisitor implements ShopVisitor {
    private Map<String, Integer> stocks = new HashMap<>();

    @Override
    public String report() {
        return stocks.keySet().stream()
                .map(cake -> cake + " : " + stocks.get(cake))
                .collect(Collectors.joining("\n"));
    }

    @Override
    public void collect(Cake cake) {
        String description = cake.description();
        int newStock = stocks.getOrDefault(description, 0) + cake.getStock();
        stocks.put(description, newStock);
    }
}
