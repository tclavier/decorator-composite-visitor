package com.gitlab.tclavier.cupcake.shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompositePattern {
    @Test
    public void should_create_set_with_2_cupcakes() {
        final Assortment assortment = new Assortment(Shop.getInstance().doubleChocolat(), Shop.getInstance().speciality());
    }

    @Test
    public void price_of_assortment_should_be_10pct_lower() {
        Cake doubleChocolat = Shop.getInstance().doubleChocolat();
        Cake speciality = Shop.getInstance().speciality();
        final Assortment assortment = new Assortment(doubleChocolat, speciality);
        double expected = (doubleChocolat.price() + speciality.price()) * 0.9;
        assertEquals(expected, assortment.price(), 1e-10);
    }

    @Test
    public void should_add_one_cake_and_one_assortment_in_assortment() {
        Cake doubleChocolat = Shop.getInstance().doubleChocolat();
        Cake speciality = Shop.getInstance().speciality();

        final Assortment firstSet = new Assortment(doubleChocolat, speciality);
        final Assortment newSet = new Assortment(new CupCake(), firstSet);
    }

    @Test
    public void reduction_of_assortment_should_be_compute_only_ones() {
        Cake doubleChocolat = Shop.getInstance().doubleChocolat();
        Cake speciality = Shop.getInstance().speciality();
        final Assortment firstSet = new Assortment(doubleChocolat, speciality);
        CupCake cake = new CupCake();
        final Assortment newSet = new Assortment(cake, firstSet);
        double expected = (doubleChocolat.price() + speciality.price() + cake.price()) * 0.9;
        assertEquals(expected, newSet.price(), 1e-10);
    }


}
