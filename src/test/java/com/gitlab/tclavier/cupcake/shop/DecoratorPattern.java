package com.gitlab.tclavier.cupcake.shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DecoratorPattern {
    @Test
    void should_create_a_cupcake() {
        final Cake cupCake = new CupCake();
        assertEquals("CupCake", cupCake.description());
    }
    @Test
    void should_create_a_decorated_cupcake() {
        final Cake chocolatCupCake = new Chocolat(new CupCake());
        assertEquals("CupCake with Chocolat", chocolatCupCake.description());

        final Cake nutsCupCake = new Nuts(new CupCake());
        assertEquals("CupCake with Nuts", nutsCupCake.description());
    }

    @Test
     void should_create_cupcake_with_multiple_decorations() {
        final Cake cake = new Nuts(new Chocolat(new CupCake()));
        assertEquals("CupCake with Chocolat and Nuts", cake.description());

        final Cake otherCake = new Chocolat(new Nuts(new CupCake()));
        assertEquals("CupCake with Nuts and Chocolat", otherCake.description());
    }

    @Test
    void should_create_other_cake_with_decoration() {
        final Cake muffin = new Chocolat(new Muffin());
        assertEquals("Mufin with Chocolat", muffin.description());
    }

    @Test
    public void should_get_price_of_cake(){
        final Cake muffin = new Muffin();
        assertEquals(4.5, muffin.price(), 1e-10);
        final Cake cupCake = new CupCake();
        assertEquals(2.5, cupCake.price(), 1e-10);
    }

    @Test
    public void should_get_price_of_decorated_cupcake(){
        final Cake chocolatCupCake = new Chocolat(new CupCake());
        assertEquals(2.5 + 1, chocolatCupCake.price(), 1e-10);
        final Cake otherCake = new Chocolat(new Nuts(new CupCake()));
        assertEquals(2.5 + 1 + 0.9, otherCake.price(), 1e-10);
    }
}
