package com.gitlab.tclavier.cupcake.shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FactoryPattern {

    @Test
    void should_create_speciality() {
        Cake speciality = Shop.getInstance().speciality();
        assertEquals("CupCake with Chocolat and Nuts and Chocolat", speciality.description());
    }

    @Test
    void should_create_double_chocolat_cupcake() {
        Cake doubleChocolat = Shop.getInstance().doubleChocolat();
        assertEquals("CupCake with Chocolat and Chocolat", doubleChocolat.description());
    }
}
