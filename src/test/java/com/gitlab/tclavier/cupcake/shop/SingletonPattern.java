package com.gitlab.tclavier.cupcake.shop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SingletonPattern {

    @Test
     void factory_should_be_uniq (){
        assertEquals(Shop.getInstance(), Shop.getInstance());
    }

    @Test
    void factory_should_be_not_null (){
        assertNotNull(Shop.getInstance());
    }

    @Test
    void should_not_invoque_constructor() {
        //new Shop();
    }
}
