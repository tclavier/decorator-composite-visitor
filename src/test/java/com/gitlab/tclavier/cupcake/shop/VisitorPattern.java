package com.gitlab.tclavier.cupcake.shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class VisitorPattern {
    @Test
    void should_visite_cake_to_get_stocks() {
        ShopVisitor visitor = new StockVisitor();
        Cake cake = cookCupCake(5);
        cake.accept(visitor);
        Assertions.assertEquals("CupCake : 5", visitor.report());
    }

    @Test
    void should_visite_cakes_to_get_stocks() {
        ShopVisitor visitor = new StockVisitor();
        Cake cake1 = cookCupCake(5);
        Cake cake2 = cookDoubleChocolat();

        cake1.accept(visitor);
        cake2.accept(visitor);
        Assertions.assertEquals("CupCake : 5\nCupCake with Chocolat and Chocolat : 15", visitor.report());
    }

    @Test
    void should_depublicate_stocks() {
        ShopVisitor visitor = new StockVisitor();
        Cake cake1 = cookCupCake(5);
        Cake cake2 = cookCupCake(15);

        cake1.accept(visitor);
        cake2.accept(visitor);
        Assertions.assertEquals("CupCake : 20", visitor.report());
    }

    @Test
    void should_visite_cake_to_get_price() {
        ShopVisitor visitor = new PriceVisitor();
        Cake cake = cookDoubleChocolat();
        cake.accept(visitor);
        Assertions.assertEquals("# Menu\n* CupCake with Chocolat and Chocolat : 4.5 €", visitor.report());
    }

    @Test
    void should_visite_cakes_to_get_prices() {
        ShopVisitor visitor = new PriceVisitor();
        Cake cake1 = new CupCake();
        Cake cake2 = cookDoubleChocolat();

        cake1.accept(visitor);
        cake2.accept(visitor);
        Assertions.assertEquals("# Menu\n* CupCake : 2.5 €\n* CupCake with Chocolat and Chocolat : 4.5 €", visitor.report());
    }

    private Cake cookCupCake(int stock) {
        Cake cake = new CupCake();
        cake.setStock(stock);
        return cake;
    }

    private Cake cookDoubleChocolat() {
        Cake cake2 = Shop.getInstance().doubleChocolat();
        cake2.setStock(15);
        return cake2;
    }
}
